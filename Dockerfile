FROM ruby:3.1.3
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
ARG BITBUCKET_APP_PASSWORD
ARG BITBUCKET_USERNAME
RUN bundle config bitbucket.org $BITBUCKET_USERNAME:$BITBUCKET_APP_PASSWORD && bundle install
# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
ENV PORT 3000
EXPOSE 3000
WORKDIR /myapp
# Configure the main process to run when running the image
CMD ["rails", "server", "-b", "0.0.0.0"]