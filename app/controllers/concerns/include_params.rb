module IncludeParams
  def include_params
    params[:includes]&.split(",") || []
  end

  def exclude_params
    params[:excludes]&.split(",") || []
  end

  def format(data)
    data.to_hash(user_options: {
                   include: include_params,
                   exclude: exclude_params
                 })

  end
end
