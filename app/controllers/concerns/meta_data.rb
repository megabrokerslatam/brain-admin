# frozen_string_literal: true

module MetaData
    def pagination(data)

      {
        total_count: data.total_count,
        items_per_page: data.size,
        current_page: data.current_page,
        total_pages: data.total_pages,
      }
    end
  end
  