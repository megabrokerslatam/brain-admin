module Api
  module V1
    class AgentsController < ApiController
      before_action :set_agent, only: %i[show update destroy]
      def index

        @agents = Agent.all
      end

      def create
        @employee = Agent.new(plan_params)
        if !@agent.save
          render json: { "errors": @agent.errors.full_messages, "params": params }
        else
          render 'show', status: :created
        end
      end

      def update
        if !@agent.update(agent_params)
          render json: { "errors": @agent.errors.full_messages }
        else
          render 'show', status: :ok
        end
      end

      def show
        
      end

      def destroy
        if @agent.destroy
          head :ok
        else
          head :not_found
        end
      end

      private

      def set_agent
        @agent = Agent.find(params[:id])
      end

      def plan_params
        params.permit(:first_name, :last_name, :email)
      end
    end
  end
end
