module Api
  module V1
    class UsersController < ApiController
      def index
        @users = User.all
        render json: @users
      end

    

      def show; end

      def create; end

      def update; end

      def delete; end
    end
  end
end
