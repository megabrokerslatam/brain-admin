class Api::V1::ClientsController < Api::V1::ApiController
  include MetaData
  include IncludeParams

  before_action :set_client, only: %i[show update destroy]

  def index
    clients = Client.with(include_params).filter_by(filter_params).order(id: :asc).page(params[:page] || 1).per(params[:per_page] || 5)
    render json: { data: format(ClientRepresenter.for_collection.new(clients)), pages: pagination(clients) }
  end

  def create
    result = Client::Operation::Create.call(params: client_params)
    if result.success?
      render json: { data: format(ClientRepresenter.new(result[:model])) }, status: :created
    else
      render json: { "errors": result[:errors] }, status: :unprocessable_entity
    end
  end

  def update
    if !@client.update!(client_params)
      render json: { "errors": @client.errors.full_messages }
    else
      render json: { data: format(ClientRepresenter.new(@client)) }, status: :ok
    end
  end

  def show
    render json: { data: format(ClientRepresenter.new(@client)) }
  end

  def destroy
    if @client.destroy
      head :ok
    else
      head :not_found
    end
  end

  private

  def set_client
    @client = Client.with(include_params).find(params[:id])
  end

  def filter_params
    params.permit(:name)
  end

  def client_params
    params.permit(
      :first_name,
      :last_name,
      :phone_number,
      :email,
      :files,
      :birthday,
      :city,
      :country,
      :commercial_executive_id,
      :support_executive_id,
      :h_id
    )
  end
end
