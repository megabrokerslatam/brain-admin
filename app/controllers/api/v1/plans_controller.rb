
    class Api::V1::PlansController < Api::V1::ApiController
      include MetaData
      include IncludeParams
      before_action :set_plan, only: %i[show update destroy]
      def index
        @plans = Plan.all
        render json:PlanRepresenter.for_collection.new(@plans).to_hash(user_options: {include:include_params}) 
      end

      def create
        @plan = Plan.new(plan_params)
        if !@plan.save
          render json: { "errors": @plan.errors.full_messages, "params": params }
        else
          render 'show', status: :created
        end
      end

      def update
        if !@plan.update(plan_params)
          render json: { "errors": @plan.errors.full_messages }
        else
          render 'show', status: :ok
        end
      end

      def show; end

      def destroy
        if @plan.destroy
          head :ok
        else
          head :not_found
        end
      end

      private

      def set_plan
        @plan = Plan.find(params[:id])
      end

      def plan_params
        params.permit(:name, :short_name, :portal_url, :company_id, :joint)
      end
    end
