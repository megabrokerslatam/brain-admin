module Api
  module V1
    class EmployeesController < ApiController
      before_action :set_employee, only: %i[show update destroy]
      def index
        @employees = Employee.all
      end

      def create
        @employee = Employee.new(plan_params)
        if !@employee.save
          render json: { "errors": @employee.errors.full_messages, "params": params }
        else
          render 'show', status: :created
        end
      end

      def update
        if !@employee.update(employee_params)
          render json: { "errors": @employee.errors.full_messages }
        else
          render 'show', status: :ok
        end
      end

      def show; end

      def destroy
        if @employee.destroy
          head :ok
        else
          head :not_found
        end
      end

      private

    
      def set_employee
        @employee = Employee.find(params[:id])
      end

      def plan_params
        params.permit(:first_name, :last_name, :email)
      end
    end
  end
end
