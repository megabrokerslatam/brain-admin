class Api::V1::PoliciesController < Api::V1::ApiController
  before_action :set_policy, only: %i[show update destroy]
  include MetaData
  include IncludeParams

  def index
    policies = Policy.with(include_params).filter_by(filter_params).page(params[:page] || 1).per(50)
    render json: {
      data: PolicyRepresenter.for_collection.new(policies).to_hash(user_options: {include:include_params}),pages: pagination(policies),
      counters: Policy.grouped_by_company,
    }
  end

  def create
  
    result = Policy::Operation::Create.call(params: policy_params,renewal:renewal_params)
    if result.success?
      policy = result[:model]
      render json: {
        data: PolicyRepresenter.new(policy),
      }, status: :created
    else

      render json: result['contract.default'].errors, status: :unprocessable_entity
    end
  end

  def update
    if !@policy.update(policy_params)
      render json: { "errors": @policy.errors.full_messages }
    else
      render "show", status: :ok
    end
  end

  def show
    render json: {data: PolicyRepresenter.new(@policy).to_hash(user_options: {include:include_params})}
  end

  def destroy
    if @policy.destroy
      head :ok
    else
      head :not_found
    end
  end

  private

  def set_policy
    @policy = Policy.find(params[:id])
  end

  def policy_params
    params.permit(:effective_date, :policy_number, :client_id, :company_id, :agent_id)
  end

  def renewal_params
    params.permit(:plan_id, :option, :premium, :frequency, :renewal_date)
  end

  def filter_params
    params.permit(
      :company_id,
      :client_id,
      :company
    )
  end
end
