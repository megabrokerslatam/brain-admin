class Api::V1::ApplicationController < ActionController::Base
    before_action :allow_cors

    def allow_cors
     


      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
      headers['Access-Control-Request-Method'] = '*'
      headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    end
  
    def set_admin_locale
      I18n.locale = :es
    end

    
   
  end
  