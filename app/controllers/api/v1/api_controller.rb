require "psauth"

class Api::V1::ApiController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  skip_before_action :verify_authenticity_token
  before_action :check_authorization

  def check_authorization
    Psauth::AuthServer.call(params, request.headers)
  end

  rescue_from Psauth::AuthServerException do |exception|
    data = exception.detail.symbolize_keys!

    code = data[:state]
    render json: exception.detail, status: code.to_sym
  end

  def record_not_found(exc)
    render json: { error: "No se pudo encontrar el registro" }, status: :not_found
  end
end
