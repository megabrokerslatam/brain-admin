class Api::V1::RenewalsController < Api::V1::ApiController
      include MetaData
      include IncludeParams
      before_action :set_renewal, only: %i[show update delete]
      def index
     
        renewals =  Renewal.running.with(include_params).filter_by(filter_params).order(id: :asc).page(params[:page] || 1).per(params[:per_page]||1)
        render json: {
          data: RenewalRepresenter.for_collection.new(renewals),
          pages: pagination(renewals),
          counters: Renewal.grouped_by_company,
        }
      end

      def create
        result = Renewal::Operation::Create.call(params: renewal_params)
        if result.success?
          @renewal = result[:model]
          render json: RenewalRepresenter.new(@renewal), status: :created
        else
          render json: { "errors":  result['contract.default'].errors }, status: :unprocessable_entity
        end
      end


      def update
        Renewal.tracking_changes(actor: Agent.first) do
        result = Renewal::Operation::Update.call(params: renewal_params)
        if result.success?
          @renewal = result[:model]
          render json: RenewalRepresenter.new(@renewal), status: :ok
        else
          render json: { "errors": result['contract.default'].errors }, status: :unprocessable_entity
        end
      end
      
      end

      def show
        render json: RenewalRepresenter.new(@renewal)
      end

      def set_renewal
        @renewal = Renewal.find(params[:id])
      end

      private

      def filter_params
        params.permit(:policy_id,:company,:client_id,:filter_by_renewal_date_lt,:renewal_date_gt)
      end

      def renewal_params
        params.permit(:id, :plan_id, :policy_id, :option, :frequency, :premium, :renewed_on, :collection_state,:payment_state,:renewal_date)
      end
    end
