module Api
  module V1
    class CompaniesController < ApiController
      before_action :set_company, only: %i[show update destroy]
      def index
        @companies = Company.all
      end

      def create
        @company = Company.new(company_params)
        if !@company.save
          render json: { "errors": @company.errors.full_messages }
        else
          render 'show', status: :created
        end
      end

      def update
        if !@company.update(company_params)
          render json: { "errors": @company.errors.full_messages }
        else
          render 'show', status: :ok
        end
      end

      def show; end

      def destroy
        if @company.destroy
          head :ok
        else
          head :not_found
        end
      end

      private

      def set_company
        @company = Company.find(params[:id])
      end

      def company_params
        params.permit(:name, :short_name, :portal_url)
      end
    end
  end
end
