class UpdatePolicyStatusJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    Policy.all.each do |p|
      renewal = p.last_renewal

      next unless renewal

      p.update_attributes!(status: 'vencida') if renewal.end_date < Time.now
    end
  end
end
