

json.policies @policies do |policy|
    json.(policy,:id,:client_id,:company_id,:policy_number,:effective_date,:status,:enabled)
    json.company policy.company.name || 'N/A'
    json.client_name policy.client.first_name
    json.plan policy.last_renewal&.plan&.name || 'N/A'
    json.option policy.last_renewal&.option || 'N/A'
    json.status policy.last_renewal&.state || 'N/A'
    json.renewals policy.renewals
end
json.current_page @policies.current_page
json.items_per_page @policies.limit_value
json.total_pages @policies.total_pages
json.total_count @policies.total_count
json.counters do
    Company.all.each do |company|
        json.set! company.name, company.policies.count 
    end
end





