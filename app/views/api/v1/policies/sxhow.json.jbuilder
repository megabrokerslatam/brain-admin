
json.(@policy, 
    :id,
    :client_id,
    :company_id,
    :policy_number,
    :effective_date,
    :status,
    :enabled
)
json.client_name @policy.client.first_name
json.company @policy.company.name || 'N/A'
json.plan @policy.last_renewal&.plan&.name || 'N/A'
json.option @policy.last_renewal&.option || 'N/A'
json.renewals @policy.renewals
