json.clients @clients do |client|
    json.age client.birthday.nil? ? nil : Date.current.year - client.birthday.year
    json.(client, :id,:first_name,:last_name,:phone_number,:email,:birthday,:city,:country,:commercial_executive_id,:support_executive_id,:hubspot,:titular)

    json.policies client.policies do |policy|
        json.id policy.id
        json.company policy.company.name
        json.policy_number policy.policy_number
        json.option policy.renewals.last&.option || 'No renewals for this policy'
        json.plan policy.renewals.last&.plan&.name || "No Renewals for this policy"

    end
end
json.current_page @clients.current_page
json.items_per_page @clients.limit_value
json.total_pages @clients.total_pages
json.total_count @clients.total_count