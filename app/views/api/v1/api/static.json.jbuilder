json.agents @agents do |a|
    json.first_name a.first_name
    json.id a.id
end

json.collectors @collectors do |c|
    json.first_name c.first_name
end

json.companies @companies do |company|
    json.company company.name
end

