ActiveAdmin.register Agent do
  menu label: "Agentes"
  index do 
    id_column
    column :id
    column :first_name
    column :last_name
    column :email
    column :phone_number
    actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  # permit_params :legacy_id, :first_name, :last_name, :phone_number, :email, :type, :birthday, :city, :country, :commercial_executive_id, :support_executive_id, :agent_id, :h_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:legacy_id, :first_name, :last_name, :phone_number, :email, :type, :birthday, :city, :country, :commercial_executive_id, :support_executive_id, :agent_id, :h_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
