ActiveAdmin.register Renewal do

  menu label: "Renovaciones"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :policy_id, :plan_id, :legacy_id, :option, :frequency, :premium, :renewal_date
  scope "Todas", :all
 
  scope "Nuevas" do |s|
    s.where("collection_state  = ?",'new')
  end
  scope "Running" do |s|
    s.where("collection_state  != ?",'new')
  end
  
 
  #
  # or
  #
  # permit_params do
  #   permitted = [:policy_id, :plan_id, :legacy_id, :option, :frequency, :premium, :renewed_on, :renewal_date, :status, :state]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  index do
    id_column
    column "Poliza" do |r|
      link_to(r.policy.policy_number, admin_policy_path(r.policy.id))
    end
    column "Cliente", :client
    column :plan
    column "Opcion", :option
    column "Frecuencia", :frequency
    column "Prima", :premium
    column "Fecha Renovacion", :renewal_date
    column "Estado Cobro", :collection_state
    column "Estado Pago", :payment_state
    column "F. Creacion", :created_at
    actions
  end

  form do |f|
    inputs "Renovacion" do
      f.input :policy_id, :label => "Poliza", :as => :select, :collection => Policy.all.map { |u| ["#{u.policy_number}", u.id] }
      f.input :plan_id, :label => "Plan", :as => :select, :collection => Plan.all.map { |u| ["#{u.name}", u.id] }
      f.input :option
      f.input :frequency, :label => "Frecuencia", :as => :select, :collection => %w[Annual Semiannual Quarterly Monthly].map { |u| ["#{u}", u] }
      f.input :premium
      f.input :renewal_date
    end
    actions
  end
end
