ActiveAdmin.register Company do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :legacy_id, :short_name, :portal_url, :logo
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :legacy_id, :short_name, :portal_url]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
    index do 
      
    end

    form title: 'A custom title' do |f|
      inputs 'Details' do
       
        input :name, label: "Nombre"
        input :short_name, label: "Nombre Corto"
        input :portal_url, label: "Pagina web"
    

        f.inputs "Logo", class: 'logo_preview', title: 'Upload' do
          input :logo, as: :file ,label: "Archivo"
          
        end
      end
      
      actions
    end

  
end
