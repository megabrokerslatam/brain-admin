ActiveAdmin.register Policy do
  menu label: "Polizas"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  # permit_params :client_id, :legacy_id, :company_id, :policy_number, :effective_date, :status, :enabled
  #
  # or
  #
  # permit_params do
  #   permitted = [:client_id, :legacy_id, :company_id, :policy_number, :effective_date, :status, :enabled]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  index do
    id_column
    column "Aseguradora", :company, sortable: :company_id
    column "Cliente", :client, sortable: :client_id
    column "# Poliza", :policy_number
    column "Fecha Efectiva", :effective_date
    column "Status", :status
    actions
  end

  show title: proc {|policy| "Poliza ##{policy.policy_number}"} do 
    attributes_table do
      row :id
      row "Aseguradora" do |a|
        a.company
      end
      row "Cliente" do |p|
        p.client
      end
      row "Fecha Efectiva" do |p|
        p.effective_date
      end

      row "Status" do |p|
        p.status
      end

      row "Agente" do |p|
        p.agent
      end

      row "Fecha de Renovacion" do |p|
        p.current_renewal.renewal_date
      end
    end

    panel "Renovaciones" do
      table_for policy.renewals.order(renewal_date: "DESC") do 
        column :id
        column :plan
        column "Fecha Renovacion" do |r|
          r.renewal_date
        end
        column "Estado de Cobro" do |r|
          r.collection_state
        end
        column "Estado de pago" do |r|
          r.payment_state
        end
      end
    end
  end
  
end
