# frozen_string_literal: true

class BaseWorker
    def self.call(*args)
      new(*args).call
    end
  end
  