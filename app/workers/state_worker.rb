class StateWorker < BaseWorker
  def initialize
    @renewals = Renewal.all
  end

  def call
    @renewals.each do |renewal|
      status = get_renewal_status(renewal)
      if status != 'paid' # Wont run if paid
        renewal.payment_state = status
        renewal.save!
      end
    end
  end

  private
  def get_renewal_status(renewal)
    # If it`s paid we dont change the status
    return renewal.payment_state unless !['paid','new'].include? renewal.payment_state
    # if it has already gone overdue (will go from overdue -> expired)
    return 'expired' if renewal.days_left < -60 && renewal.payment_state == 'overdue'
    # will go pending -> overdue
    return 'overdue' if renewal.days_left < 0 && renewal.payment_state == 'pending'
    return renewal.payment_state
  end
end
