module Renewal::Operation
  class Create < Trailblazer::Operation
    class Present < Trailblazer::Operation
      step Model(Renewal, :new)
      step Contract::Build(constant: Renewal::Contract::Create)
    end
    step Subprocess(Present)
    step Contract::Validate()
    step Contract::Persist()
  end
end
