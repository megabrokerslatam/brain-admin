module Renewal::Operation
  class Update < Trailblazer::Operation
    class Present < Trailblazer::Operation
      step Model(Renewal, :find_by)
      step Contract::Build(constant: Renewal::Contract::Update)
    end

    

    
    step Subprocess(Present)
    step Contract::Validate(), fail_fast: true
    step Contract::Persist()
  end
end
