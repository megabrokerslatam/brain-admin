module Renewal::Contract
  class Update < Reform::Form
    property :policy_id
    property :plan_id
    property :option
    property :frequency
    property :premium
    property :renewed_on
    property :renewal_date
    property :collection_state
    property :payment_state
    validates :policy_id, presence: true
    validates :plan_id, presence: true
    validates :option, presence: true
    validates :frequency, presence: true
    validates :premium, presence: true
  end
end
