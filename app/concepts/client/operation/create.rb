module Client::Operation
    class Create < Trailblazer::Operation
      class Present < Trailblazer::Operation
        step Model(Client, :new)
        step Contract::Build(constant: Client::Contract::Create)
      end
  
      step Subprocess(Present)
      step Contract::Validate()
      fail :validation_failed, fail_fast: true
      step Contract::Persist()
      fail :save_failed, fail_fast: true
  
      def save_failed(ctx, **)
        errors = ctx[:model].errors.full_messages
        ctx[:errors] = errors.map { |field, msg| "#{field.to_s.capitalize}#{msg}" }
      end
  
      def validation_failed(ctx, **)
        errors = ctx['contract.default'].errors.messages
        ctx[:errors] = errors.map { |field, msg| "#{field.to_s.capitalize} #{msg.first}" }
      end
    end
  end
  