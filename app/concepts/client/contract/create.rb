module Client::Contract
    class Create < Reform::Form
      property :first_name
      property :last_name
      property :phone_number
      property :email
      property :birthday
      property :city
      property :country
      property :commercial_executive_id
      property :support_executive_id
      validates :first_name,:email,:birthday, presence: true
    end
  end
  