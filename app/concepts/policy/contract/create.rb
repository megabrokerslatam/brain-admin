module Policy::Contract
  class Create < Reform::Form
    property :policy_number
    property :agent_id
    property :client_id
    property :company_id
    property :effective_date
    validates :effective_date, :company_id, :agent_id, :policy_number, presence: true
    validate :client?, :company?, :agent?



    def agent?
      errors.add("agent", "is not valid") unless Agent.find_by_id(agent_id)
    end

    def client?
      errors.add("Client", "is not valid") unless Client.find_by_id(client_id)
    end

    def company?
      errors.add("Company", "is not valid") unless Company.find_by_id(company_id)
    end
  end
end
