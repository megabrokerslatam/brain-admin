PolicyModel = Policy
module Policy::Operation
  class Create < Trailblazer::Operation
    class Present < Trailblazer::Operation
      step Model(PolicyModel, :new)
      step Contract::Build(constant: PolicyModel::Contract::Create)
    end
    step Subprocess(Present)
    step Contract::Validate(), fail_fast: true
    step Wrap(Transaction) {
      step Contract::Persist()
      step :create_renewal
    }

    def create_renewal(ctx,renewal:,**)
      old_renewal = Renewal.create(renewal.merge({policy_id:ctx[:model].id,collection_state:'collected',payment_state:'paid'}))
      result = Renewal::Operation::Create.call({params:renewal.merge({policy_id:ctx[:model].id})})
      result.success?
    end
   
  end
end
