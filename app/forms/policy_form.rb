class PolicyForm
  include ActiveModel::Model
  validates :renewal, presence: true

  def initialize(policy, renewal)
    @policy = Policy.new(policy)
    @renewal = @policy.renewals.build(renewal)
  end
end
