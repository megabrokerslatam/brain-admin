require 'representable/json'
class ClientRepresenter < Representable::Decorator
    include Representable::JSON
    
    property :id
    property :first_name
    property :last_name
    property :phone_number
    property :email
    property :birthday
    property :city
    property :country
    property :commercial_executive_id
    property :support_executive_id
    property :hubspot
    property :titular
    property :age, exec_context: :decorator
    property :balance, exec_context: :decorator, if: -> (user_options:,**){!user_options[:exclude]&.include? 'balance'}
    property :h_id

    def h_id
        represented.h_id || "N/A"
    end

    collection :policies, extend: PolicyRepresenter, class: Policy, if: -> (user_options:,**){user_options[:include]&.include? 'policies'}


    def age
        represented.birthday.nil? ? nil : Date.current.year - represented.birthday.year
    end

    def balance
        b= SiscobClient.get_client_info(represented.id).slice(:USD,:BOB)
        {:USD=>b[:USD].to_f,:BOB=>b[:BOB].to_f}
    end
end
