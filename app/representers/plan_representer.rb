require 'representable/json'
class PlanRepresenter < Representable::Decorator
    include Representable::JSON
    property :id
    property :name
    property :company_id
end

