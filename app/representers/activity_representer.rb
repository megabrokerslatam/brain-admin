require 'representable/json'
module ActivityRepresenter
    include Representable::JSON
    property :actor do
        property :id
        property :first_name
    end
    property :attribute_changes
    property :created_at
end

