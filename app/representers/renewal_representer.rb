require 'representable/json'
class RenewalRepresenter < Representable::Decorator
    include Representable::JSON
    property :id
    property :policy_id
    property :plan_id
    property :option
    property :frequency
    property :premium
    property :renewal_date
    property :renewed_on
    property :collection_state
    property :payment_state
    property :next_payment_date
    property :client_id, exec_context: :decorator
    property :plan, exec_context: :decorator
    property :period, exec_context: :decorator
    property :policy, exec_context: :decorator
    property :owner, exec_context: :decorator
    property :company, exec_context: :decorator
    property :current_debt, exec_context: :decorator
    collection :activities, extend: ActivityRepresenter, class: ActsAsTracked::Activity
    
    def current_debt
        payed_amount = SiscobClient.get_renewal_info(represented.id)["payed_amount"]
        represented.premium.to_f - payed_amount.to_f
    end

    def client_id
        represented.policy.client.id
    end

    def company
        represented.company.name
    end
    def policy
        represented.policy.policy_number
    end
    def plan
        represented.plan.name
    end
    def owner
        represented.policy.client.full_name
    end

    def period
        "#{represented.renewal_date.year - 1} - #{represented.renewal_date.year}"
    end

end

