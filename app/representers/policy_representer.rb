require 'representable/json'
class PolicyRepresenter < Representable::Decorator
    include Representable::JSON
    property :id
    property :policy_number
    property :effective_date
    property :status
    property :enabled

    property :company, exec_context: :decorator
    property :plan, exec_context: :decorator
    property :option, exec_context: :decorator
    property :client, extend: ClientRepresenter, class: Client, if: -> (user_options:,**){user_options[:include]&.include? 'client'}
    collection :renewals, extend: RenewalRepresenter, class: Renewal, if: -> (user_options:,**){user_options[:include]&.include? 'renewals'}
    property :dependants, extend: ClientRepresenter, class: Dependant, if: -> (user_options:,**){user_options[:include]&.include? 'dependants'}

    def company
        represented.company.name || 'N/A'
    end
    
    def plan
        represented.last_renewal&.plan&.name || "N/A"
    end

    def option
        represented.last_renewal&.option || 'N/A'
    end
end
