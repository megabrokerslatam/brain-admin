require 'representable/json'
module PaginationRepresenter
    include Representable::JSON
    property :current_page, exec_context: :decorator
    property :items_per_page
    property :total_pages
    property :total_count
end

