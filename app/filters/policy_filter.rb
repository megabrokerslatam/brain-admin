
class PolicyFilter
  def self.call(params)
    new(params).policies # la keyword new llama al constructor de la clase, en rails llamamos al constructor como "initialize"
  end

  def initialize(options)
    @options = options
    @policies = Policy.all
    filter_by_company
    filter_by_client
    filter_by_company_name
  end

  def policies
    @policies
  end

  
  private
  def filter_by_company
    if @options[:company_id]
      @policies = Policy.where(company_id: @options[:company_id])
    end
  end

  def filter_by_company_name
  
    if @options[:company] && @options[:company] != ""

      @policies = Company.find_by(name: @options[:company]).policies
    end
  end

  def filter_by_client
    if @options[:client_id]
      @policies = Policy.where(client_id: @options[:client_id])
    end
  end
end
