class RenewalFilter
  def self.call(params)
    new(params).filter
  end

  def filter
    filter_by_company
    filter_by_policy_id
    filter_by_status
    @renewals
  end

  private

  def filter_by_policy_id
    if(@options[:policy_id])
      @renewals = @renewals.where(policy_id: @options[:policy_id])
    end
  end

  def filter_by_company
    if @options[:company]
      c = Company.find_by_name(@options[:company])
      if c
        @renewals = c.renewals
      else
        @renewals = []
      end
    end
  end

  def filter_by_status 
    if (@options[:status])
      @renewals = @renewals.where(status: @options[:status])
    end
  end

  def initialize(options)
    @renewals = Renewal.all.order(id: 'ASC')
    @options = options
  end
end
