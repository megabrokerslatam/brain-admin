class Region < ApplicationRecord
  belongs_to :company
  has_many :plans
  has_and_belongs_to_many :countries

  def to_s
    name
  end
end
