class User < ApplicationRecord
  validates :first_name, presence: true

  def full_name
    first_name + ' ' + (last_name || '')
  end

  def to_s
    full_name
  end
end
