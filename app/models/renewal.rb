class Renewal < ApplicationRecord
  acts_as_tracked
  include Filterable
  include Includable
  include ActionView::Helpers::NumberHelper
  belongs_to :policy
  belongs_to :plan
  has_one :client, through: :policy
  has_one :company, through: :policy

  after_update :fire_new
  before_update :set_running

  scope :filter_by_client_id, ->(client_id) {
          joins(:policy).joins(:client).where(policies: { client_id: client_id })
        }
  scope :filter_by_company, ->(company) {
          joins(:policy).joins(:company).where(companies: { name: company })
        }
  scope :filter_by_renewal_date_gt, ->(date) {
          where("renewal_date > ?", date)
        }
  scope :filter_by_renewal_date_lt, ->(date) {
          where("renewal_date < ?", date)
        }

  scope :grouped_by_company, ->() {
          joins(:policy).joins(:company).group("companies.name").count
        }

  scope :running, ->() {
          where("payment_state != ?", "new")
  }

  def next_payment_date
    start_date = renewal_date - 1.year
    case frequency
    when "Annual"
      renewal_date
    when "Semiannual"
      if start_date + 6.months > Time.now
        start_date + 6.months
      else
        renewal_date
      end
    when "Quarterly"
      if start_date + 3.months > Time.now
        start_date + 3.months
      elsif start_date + 6.months > Time.now
        start_date + 6.months
      elsif start_date + 9.months > Time.now
        start_date + 9.months
      else
        renewal_date
      end
    when "Monthly"
      (1..11).each do |x|
        if start_date + x.months > Time.now
          return start_date + x.months
        end
      end
      renewal_date
    end
  end

  # How many days until renewal date
  def days_left
    (renewal_date.to_date - DateTime.current.to_date).to_i
  end


  def set_running
    if will_save_change_to_premium?
     
      self.collection_state = 'pending'
      self.payment_state = 'pending'
    end
  end

  def fire_new
    if payment_state == 'paid'
      r = Renewal.new(self.attributes.except("id"))
      r.collection_state = "new"
      r.payment_state = "new"
      r.save!
      r
    end
   
  end
end
