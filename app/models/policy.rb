class Policy < ApplicationRecord
  include Filterable
  include Includable
  has_many :renewals, dependent: :destroy
  has_many :dependants
  has_one :first_renewal, class_name: :Renewal

  belongs_to :client
  belongs_to :company
  belongs_to :agent

  scope :filter_by_company, ->(company) { joins(:company).where(companies: { name: company }) }
  scope :filter_by_client_id, ->(client_id) { where(client_id: client_id) }
  scope :filter_by_company_id, ->(company_id) { where(company_id: company_id) }

 
  # And now the grouping
  scope :grouped_by_company, ->() {
          joins(:company).group("companies.name").count
        }

  enum policy_type: {
    health: 0,
    life: 1,
    kindapping: 2,
  }

  def current_renewal
    renewals.where("payment_state != ?", "new").order(created_at: "DESC").first
  end

  def status
    if current_renewal
      if current_renewal.payment_state == "overdue"
        "vencida"
      else
        "activa"
      end
    else
      'nueva'
    end
  end

  def last_renewal
    renewals.last
  end

  def client_name
    self.client.first_name
  end
end
