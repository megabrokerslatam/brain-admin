class Client < User
  include Filterable
  include Includable
  has_many :policies, dependent: :destroy
  has_many :renewals, through: :policies
  belongs_to :agent, optional: true 

  scope :filter_by_name,-> (name){
    where("first_name ILIKE ? or last_name ILIKE ? " ,"%#{name}%","%#{name}%")
  }
  
  def self.with_company(company_name)
    joins(:policies).where('company_id = ?',Company.find_by(name: company_name).id)
  end

  def titular
    policy_id == nil
  end
end
