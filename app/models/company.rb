class Company < ApplicationRecord
  validates :name, presence: true
  validates :short_name, presence: true
  validates :name, uniqueness: true
  has_many :plans
  has_many :policies
  has_many :renewals, through: :policies

  has_one_attached :logo
end
