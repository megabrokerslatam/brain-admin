class SiscobClient
  @@api = ::Faraday.new(url: ENV["SISCOB_API_URL"])
  @@apikey = ENV["AUTH_SERVER_API_KEY"]
  @@app_name = ENV["APP_CODENAME"]

  def self.get_client_info(client_id)
    controller = "clients"
    action = "show"
    res = @@api.get("/api/v1/clients/#{client_id}") do |req|
      req.headers["Api-Key"] = ENV["AUTH_SERVER_API_KEY"]
      req.params["_controller"] = controller
      req.params["_action"] = action
      req.params["_app_name"] = @@app_name
    end
    @response = JSON.parse(res.body)
    if res.status != 200
      raise @response["error"]
    end
    @response.with_indifferent_access
  end

  def self.get_renewal_info(renewal_id)
    controller = "renewals"
    action = "show"
    res = @@api.get("/api/v1/renewals/#{renewal_id}") do |req|
      req.headers["Api-Key"] = ENV["AUTH_SERVER_API_KEY"]
      req.params["_controller"] = controller
      req.params["_action"] = action
      req.params["_app_name"] = @@app_name
    end
    @response = JSON.parse(res.body)

    if res.status != 200
      raise @response["error"]
    end
    @response.with_indifferent_access
  end
end
