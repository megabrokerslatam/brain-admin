# frozen_string_literal: true

class Transaction
    def self.call((_ctx), *, &block)
      ActiveRecord::Base.transaction(&block)
    end
  end
  