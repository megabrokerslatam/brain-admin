require 'faker'
Company.delete_all
Plan.delete_all
Agent.delete_all
Employee.delete_all
Client.delete_all
Policy.delete_all
Renewal.delete_all
AdminUser.delete_all
def get_random_object(className)
  className.constantize.all.pluck(:id).sample
end
# Companies
10.times do |c|
  pp "Creating companies...#{c+1}/10"
  Company.create(
    name: Faker::Company.name,
    short_name: Faker::Company.name.split.first,
    portal_url: Faker::Internet.url
  )
end

# Plans
10.times do |i|
  pp "Creating plans...#{i+1}/10"
  Plan.create(
    company_id: Company.all.pluck(:id).sample,
    name: Faker::App.name,
    joint: Faker::Boolean.boolean(true_ratio: 0.5),
    enabled: 1
  )
end

# Agents
10.times do |i|
  pp "Creating agents...#{i+1}/10"
  Agent.create(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    phone_number: Faker::PhoneNumber.phone_number,
    email: Faker::Internet.unique.email+'a',
    birthday: Faker::Date.birthday(min_age: 18,max_age:75),
    city: Faker::Address.city,
    country: Faker::Address.country
  )
end

# Employees
10.times do |i|
  pp "Creating employees...#{i+1}/10"
  Employee.create(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    phone_number: Faker::PhoneNumber.phone_number,
    email: Faker::Internet.unique.email+'e',
    birthday: Faker::Date.birthday(min_age: 18,max_age:75),
    city: Faker::Address.city,
    country: Faker::Address.country
  )
end

# Clients
10.times do |i|
  pp "Creating clients...#{i+1}/10"
  Client.create(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    phone_number: Faker::PhoneNumber.phone_number,
    email: Faker::Internet.unique.email+'c',
    birthday: Faker::Date.birthday(min_age: 18,max_age:75),
    city: Faker::Address.city,
    country: Faker::Address.country
  )
end

# Policies
20.times do |i|
  pp "Creating policies...#{i+1}/20"
  Policy.create(
    policy_number: Faker::Number.number(digits: 12),
    effective_date: Faker::Date.birthday(min_age: 2,max_age:25),
    enabled: 1,
    client_id: get_random_object('Client'),
    company_id: get_random_object('Company'),
    agent_id: get_random_object('Agent'),
    policy_type: 0
  )
end


# Renewals
50.times do |i|
  pp "Creating Renewals...#{i+1}/50"
  Renewal.create(
    plan_id: get_random_object('Plan'),
    policy_id: get_random_object('Policy'),
    option: Faker::Number.number(digits: 4),
    frequency: ['Annual','Semiannual',"Quarterly",'Monthly'].sample,
    premium: Faker::Number.number(digits: 4),
    renewed_on: Faker::Date.between(from: '2022-01-01',to: '2023-01-01'),
    renewal_date: Faker::Date.between(from: '2022-01-01',to: '2023-01-01')
  )
end


AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?