class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :legacy_id
      t.string :short_name
      t.string :portal_url
      t.timestamps
    end
  end
end
