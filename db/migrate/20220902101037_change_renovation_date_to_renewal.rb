class ChangeRenovationDateToRenewal < ActiveRecord::Migration[5.2]
  def change
    rename_column :renewals, :renovation_date, :renewal_date
  end
end
