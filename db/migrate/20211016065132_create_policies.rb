class CreatePolicies < ActiveRecord::Migration[5.2]
  def change
    create_table :policies do |t|
      t.references :client
      t.integer :legacy_id
      t.references :company
      t.string :policy_number
      t.date :effective_date
      t.string :status
      t.integer :enabled
      t.timestamps
    end
  end
end
