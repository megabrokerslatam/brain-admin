class CorrectStatusesNames < ActiveRecord::Migration[5.2]
  def change
    rename_column :renewals, :state, :collection_state
    rename_column :renewals , :status,:payment_state
  end
end
