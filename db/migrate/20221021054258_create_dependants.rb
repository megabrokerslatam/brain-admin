class CreateDependants < ActiveRecord::Migration[5.2]
  def change
    create_table :dependants do |t|
      t.integer :policy_id
      t.integer :client_id
      t.boolean :owner
      t.timestamps
    end
  end
end
