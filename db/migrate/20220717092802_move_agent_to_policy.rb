class MoveAgentToPolicy < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :agent_id
    add_column :policies, :agent_id, :integer
  end
end
