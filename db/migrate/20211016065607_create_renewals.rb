class CreateRenewals < ActiveRecord::Migration[5.2]
  def change
    create_table :renewals do |t|
      t.references :policy
      t.references :plan
      t.integer :legacy_id
      t.integer :option
      t.string :frequency
      t.string :premium
      t.date :renewed_on
      t.date :renovation_date
      t.string :status, default: 'Open'
      t.timestamps
    end
  end
end
