class ChangeDefaultStatusForRenewal < ActiveRecord::Migration[7.0]
  def change
    change_column_default :renewals, :collection_state, "new"
    change_column_default :renewals, :payment_state, "new"
  end
end
