class AddStateToRenewal < ActiveRecord::Migration[5.2]
  def change
    add_column :renewals, :state, :string
  end
end
