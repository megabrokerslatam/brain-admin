class AddPolicyTypeToPolicy < ActiveRecord::Migration[5.2]
  def change
    add_column :policies, :policy_type, :integer
  end
end
