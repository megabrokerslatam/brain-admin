class RenameHid < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :h_id, :hubspot
  end
end
