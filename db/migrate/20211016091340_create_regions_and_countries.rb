class CreateRegionsAndCountries < ActiveRecord::Migration[5.2]
  def change
    create_table :countries_regions do |t|
      t.belongs_to :region
      t.belongs_to :country
    end
  end
end
