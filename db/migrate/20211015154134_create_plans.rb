class CreatePlans < ActiveRecord::Migration[5.2]
  def change
    create_table :plans do |t|
      t.integer :legacy_id
      t.string :name
      t.boolean :joint
      t.boolean :enabled
      t.references :company
      t.timestamps
    end
  end
end
