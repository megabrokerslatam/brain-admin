class CreateRegions < ActiveRecord::Migration[5.2]
  def change
    create_table :regions do |t|
      t.references :company
      t.string :name
      t.timestamps
    end
  end
end
