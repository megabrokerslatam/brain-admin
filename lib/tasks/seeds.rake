namespace :db do
  desc 'Restore Database'
  task :restore do
    system('rails db:drop db:create db:migrate')
  end

  task :custom_seed do
    system('rails db:seed')
  end

  task seed_policies: :environment do
    puts 'Creating policies'
    10.times do |i|
      puts "Creating policies #{i}/10"
      p = Policy.create!(
        policy_number: Faker::Alphanumeric.alpha(number: 10),
        client_id: Client.order('RANDOM()').first.id,
        company_id: Company.order('RANDOM()').first.id,
        effective_date: Faker::Date.between(from: '2019-01-01', to: '2022-02-02').strftime('%Y-%m-%d')
      )
    end
  end

  task seed_renewals: :environment do
    p = Policy.all
    p.each_with_index do |policy, index|
      30.times do |i|
        puts "Creating Renewals for policy #{index} #{i}/30"
        policy.renewals.create!(
          plan_id: Plan.order('RANDOM()').first.id,
          option: Faker::Number.between(from: 1000, to: 50_000),
          frequency: %w[Annual Semiannual Quarterly Monthly].sample,
          premium: Faker::Number.between(from: 1000, to: 50_000),
          renewed_on: Faker::Date.between(from: '2019-01-01', to: '2022-02-02').strftime('%Y-%m-%d')
        )
      end
    end
  end

  task :set do
    system('rails db:restore db:seed db:seed_policies db:seed_renewals')
    puts('Setup Complete')
  end
end
