namespace :utils do
    desc 'Restore Database'
    task count_policies_without_renewals: :environment do
        count = 0
        Policy.all.each do |policy|
            if policy.renewals.count != 0 
                count = count+1
            end
        end
        puts count;
    end

    task restore_renewals: :environment do 
        Renewal.all.each do |r|
            r.collection_state = 'pending'
            r.payment_state ='pending'
            r.save!
        end
    end
end
  