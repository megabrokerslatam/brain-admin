require 'roo'
require 'json'
require 'csv'

# rails etl:execute to run rake task 

namespace :etl do
    desc 'data'

    task execute: :environment do
        year = 2021
        data = extract(year)
        # all_rows = extract_all_rows(year)
        # loaded_file_name = load_excel('all_rows', all_rows, year)
        lost_clients = extract_all_rows(year)
        loaded_file_name = load_excel('lost_clients', lost_clients, year)
        # monthly_stats = get_monthly_counts(data)
        # loaded_file_name = load_json(monthly_stats, year)
        # complete_rows = get_all_complete_rows(data)
        # loaded_file_name = load_excel('complete', complete_rows, year)
        # incomplete_rows = get_all_incomplete_rows(data)
        # loaded_file_name = load_excel('incomplete', incomplete_rows, year)
        puts loaded_file_name
    end

    def extract_all_rows(year)
        # filename = "db/seeds/renewals-#{year}.xlsx"
        filename = "db/seeds/lost-clients-#{year}.xlsx"
        data = Roo::Spreadsheet.open(filename)
        newData = []
        newData.append(['Company', 'Observations', 'Policy id', 'Legacy id', 'Client Name', 'First Name', 'Last Name', 'Plan id', 'Option', 'First effective', 'Effective date', 'Renewal Date', 'Frequency', 'Balance', 'Discount', 'Due Date', 'Agent Number', 'Agent Name', 'Power Selling Agent', 'id', 'Dependientes', 'Observacion dependiente', 'Fecha de pago', 'Forma de pago', 'Extra 1', 'Status'])
        data.sheets.each do |sheetName|
            s = data.sheet(sheetName) # gets a sheet by its name
            s.each_with_index do |row, index|
                next if index == 0
                newData.append(row)
            end
        end
        return newData
    end

    def extract(year)
        filename = "db/seeds/renewals-#{year}.xlsx"
        data = Roo::Spreadsheet.open(filename)
        
        newData = {}
        data.sheets.each do |sheetName|
            s = data.sheet(sheetName) # gets a sheet by its name

            month_rows = []
            s.each_with_index do |row, index|
                next if index == 0
                month_rows.append(transform_row(row))
            end
            newData[sheetName] = month_rows
        end
        return newData
    end

    def get_monthly_counts(data)
        # {Jan: [[...]...[]]}
        monthly_counts = {}
        total_count = 0 # resets number of renewals per month
        complete_count = 0
        incomplete_count = 0
        complete_array = []
        incomplete_array = []
        data.each do |month_name, month_rows|
            month_rows.each do |row|
                # check if it's complete
                complete = its_complete(row)
                if complete == true
                    complete_count += 1
                    complete_array.push(row)
                else
                    incomplete_count += 1
                    incomplete_array.push(row)
                end
                total_count += 1 #iterates through each renewal
            end
            monthly_counts[month_name] = {"Total Count": total_count, "Complete Count": complete_count, "Incomplete Count": incomplete_count, "Complete": complete_array, "Incomplete": incomplete_array}
        end
        return monthly_counts
    end

    def get_all_incomplete_rows(data)
        incomplete_rows = [['First Name', 'Last Name', 'Company', 'Policy id', 'Plan id', 'Option', 'Frequency', 'Premium', 'Renewal Date', 'Renewed On']]
        data.each do |month_name, month_rows|
            month_rows.each do |row|
                # check if it's complete
                complete = its_complete(row)
                if complete == false
                    incomplete_rows.push(row)
                end
            end
        end
        return incomplete_rows
    end

    def get_all_complete_rows(data)
        complete_rows = [['Client Name', 'Company', 'Policy id', 'Plan id', 'Option', 'Frequency', 'Premium', 'Renewal Date', 'Renewed On', 'Status']]
        data.each do |month_name, month_rows|
            month_rows.each do |row|
                # check if it's complete
                complete = its_complete(row)
                if complete == true
                    complete_rows.push(row)
                end
            end
        end
        return complete_rows
    end

    # def transform(raw_data)
    #     transformed_data = []
    #     raw_data.each do |item, index|
    #         clean_row = [item[2], item[6], item[7], item[11], item[12], item[10], item[10], "Won"]
    #         transformed_data.push(clean_row)
    #     end
    #     transformed_data
    # end
    
    def load_json(clean_data, year)
        file_name = "transformed_data_#{year}.json"
        File.new(file_name, "w+")
        File.open(file_name, "w+") do |f|
            f.write(clean_data.to_json)
        end
        file_name
    end

    def load_excel(name, clean_data, year)
        file_name = "#{name}_#{year}.csv"
        # File.new(file_name, "w+")
        File.write(file_name, clean_data.map(&:to_csv).join)
        file_name
    end

    def its_complete(row)
        row.each do |value|
            if value == nil
                return false
            end
        end
        return true
    end
end

def transform_row(item)
    clean_row = [item[4], item[0], item[2], item[7], item[8], item[12], item[13], item[11], item[11], item[25]]
    clean_row
end

# create_table "renewals", force: :cascade do |t|
#     t.bigint "company"                        [0] column A
#     t.bigint "policy_id"                      [2] column C
#     t.bigint "client_name"                    [4] column E
#     t.bigint "plan_id"                        [7] plan description column H
#     t.integer "legacy_id"                     do not touch
#     t.integer "option"                        [8] column I
#     t.string "frequency"                      [12] column M
#     t.string "premium"                        [13] balance column N
#     t.date "renewed_on"                       [11] column L
#     t.date "renovation_date"                  [11] column L
#     t.string "status", default: "Open"        [25] column Z
#     t.datetime "created_at", null: false
#     t.datetime "updated_at", null: false
#     t.index ["plan_id"], name: "index_renewals_on_plan_id"
#     t.index ["policy_id"], name: "index_renewals_on_policy_id"
#  end