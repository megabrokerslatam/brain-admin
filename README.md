# MB BRAIN
## Build steps
- modify `.env` file accordingly
- just `docker compose up` and you should be ready go go
## Documentation
- Generate a new test file `rails generate rspec:swagger API::V1::RandomsController` (the spec file created will rest in `spec/requests/api/v1/randoms_spec.rb`
- Generate Docs: `rake rswag:specs:swaggerize SWAGGER_DRY_RUN=0`
- Run Full testing: `bundle exec rspec` 