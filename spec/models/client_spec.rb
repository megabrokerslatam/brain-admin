require 'rails_helper'

RSpec.describe "Client Model", type: :model do
  let(:client) {build(:client)}
  let(:c) {Client.new(client.attributes)}


  context 'When the client is valid' do 
    it 'validation passes' do
      expect(c).to be_valid
    end
  end

  context "When the client has wrong attributes" do
    it 'is invalid without first name' do
      c.first_name = nil
      expect(c).to_not be_valid
      expect(c.errors.messages[:first_name]).to eq ["can't be blank"]
    end
  end
  
end
