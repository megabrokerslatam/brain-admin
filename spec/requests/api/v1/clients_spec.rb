require "swagger_helper"

RSpec.describe "Clients API", type: :request do
  include_context :stubs_context
  path "/api/v1/clients" do
    get "Get Clients" do
      tags "Clients"
      parameter name: :company, in: :query, type: :string, required: false, description: "Filter by company"
      parameter name: :includes, in: :query, type: :string, required: false, description: "Associations to add in response (comma separated values) ex: policies,renewals"
      response 200, "Clients List" do
        let(:includes) { "policies" }
        let("Api-Key") { ENV["AUTH_SERVER_API_KEY"] }
        schema "$ref" => "#/components/schemas/client_list"
        run_test!
      end
      include_examples :failed_status
    end

    post "Create client" do
      tags "Clients"
      parameter name: :client, in: :body, schema: {
        type: :object,
        properties: {
          first_name: { type: :string },
          last_name: { type: :string },
          phone_number: { type: :string },
          email: { type: :string },
          birthday: { type: :string },
          city: { type: :string },
          country: { type: :string },
          commercial_executive_id: { type: :integer },
          support_executive_id: { type: :integer },
        },
        required: ["first_name", "birthday", "email"],
      }

      response "201", "Client created" do
        let(:client) { build(:client) }
        let("Api-Key") { ENV["AUTH_SERVER_API_KEY"] }
        schema "$ref" => "#/components/schemas/client"
        run_test!
      end

      response 401, "Unauthorized" do
        security [{ api_key: [] }]
        let(:client) { build(:client) }
        let("Api-Key") { 0 }
        run_test!
      end

      context "When attributes are not valid" do
        let("apiKey") { ENV["AUTH_SERVER_API_KEY"] }
        let(:client) { build(:client) }
        it "Does not create a client without first_name" do
          client[:first_name] = nil
          post "/api/v1/clients", params: { client: client.attributes }, headers: { "Api-Key": apiKey }
          expect(response).to have_http_status(422)
        end
        it "Does not create a client without email" do
          client[:email] = nil
          post "/api/v1/clients", params: { client: client.attributes }, headers: { "Api-Key": apiKey }
          expect(response).to have_http_status(422)
        end
        it "Does not create a client without birthday" do
          client[:birthday] = nil
          post "/api/v1/clients", params: { client: client.attributes }, headers: { "Api-Key": apiKey }
          expect(response).to have_http_status(422)
        end
      end
    end
  end

  path "/api/v1/clients/{id}" do
    get "Get Client by ID" do
      tags "Clients"
      parameter name: :id, in: :path, type: :string
      response 200, "Client Info" do
        let("Api-Key") { ENV["AUTH_SERVER_API_KEY"] }
        let(:id) { create(:client).id }
        schema type: :object,
               properties: {
                 data: {
                   type: "object",
                   "$ref" => "#/components/schemas/client",
                 },

               }
        run_test!
      end
    end

    put "Update Client" do
      tags "Clients"
      parameter name: :id, in: :path, type: :string
      parameter name: :client, in: :body, schema: {
                  type: :object,
                  properties: {
                    data: {
                      type: "object",
                      "$ref" => "#/components/schemas/client",
                    },

                  },
                }

      response "200", "Client Updated" do
        let(:client) { create(:client) }
        let(:id) { client.id }
        let("Api-Key") { ENV["AUTH_SERVER_API_KEY"] }
        schema "$ref" => "#/components/schemas/client"
        run_test!
      end
    end
  end
end
