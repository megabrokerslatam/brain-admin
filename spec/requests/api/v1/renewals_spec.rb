require 'swagger_helper'

RSpec.describe 'Renewals API', type: :request do
  include_context :stubs_context

  path '/api/v1/renewals' do
    get 'Get Renewals' do 
      tags 'Renewals'
      parameter name: :policy_id, in: :query, type: :integer, required: false, description: "Filter by Policy ID"
      parameter name: :client_id, in: :query, type: :string, required: false, description: "Filter by Client ID"
      parameter name: :renovation_date_lt, in: :query, type: :string, required: false, description: "With Renovation date Prior to"
      parameter name: :renovation_date_gt, in: :query, type: :string, required: false, description: "With Renovation date After"
      parameter name: :company, in: :query, type: :string, required: false, description: "Filter By Company"
      security [{ api_key: [] }]
      response 200, 'Renewals List' do
        let('Api-Key') {ENV["AUTH_SERVER_API_KEY"]}
        schema type: :object,
        properties:{
          data:{
            type: 'array',
            items:{'$ref'=>'#/components/schemas/renewal'} 
          },
          pages:{
            '$ref'=>'#/components/schemas/pagination'
          }
        }
      
        run_test!
      end

      response 401, "Unauthorized" do 
        
        security [{ api_key: [] }]
        let('Api-Key') {0}
        run_test! 
      end
    end
  end
end
