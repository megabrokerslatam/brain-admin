require "swagger_helper"

RSpec.describe "Policies API", type: :request do
  include_context :stubs_context
  path "/api/v1/policies" do
    get "Get Policies" do
      tags "Policies"
      response 200, "Policies List" do
        let("Api-Key") { ENV["AUTH_SERVER_API_KEY"] }
        schema type: :object,
               properties: {
                 data: {
                   type: "array",
                   items: { "$ref" => "#/components/schemas/policy" },
                 },

               }
        run_test!
      end

      include_examples :failed_status
    end

    post "Create Policy" do
      tags "Policies"
      parameter name: :policy, in: :body, schema: {
        type: :object,
        properties: {
          agent_id: { type: :integer },
          client_id: { type: :integer },
          policy_number: { type: :string },
          company_id: { type: :integer },
          effective_date: { type: :string },
          renovation_date: { type: :string },
          option: { type: :string },
          frequency: { type: :string },
          plan_id: { type: :string },
          premium: { type: :string },
        },
        required: ["first_name", "birthday", "email"],
      }
      response 201, "Policy Created" do
        let("Api-Key") { ENV["AUTH_SERVER_API_KEY"] }
        let(:company) { create(:company) }
        let(:client) { create(:client) }
        let(:plan) { create(:plan) }
        let!(:agent) { create(:agent) }
        let(:policy) {
          {
            agent_id: agent.id,
            client_id: client.id,
            plan_id: plan.id,
            company_id: company.id,
            effective_date: Faker::Date.birthday(min_age: 2, max_age: 25),
            policy_number: Faker::Number.number(digits: 12),
            renewal_date: Faker::Date.birthday(min_age: 2, max_age: 25),
            option: Faker::Number.number(digits: 4),
            frequency: ["Annual", "Semiannual", "Quarterly", "Monthly"].sample,
            premium: Faker::Number.number(digits: 4),
          }
        }
        schema type: :object,
               properties: {
                 data: {
                   type: "object",
                   "$ref" => "#/components/schemas/policy",
                 },
               }
        run_test!
      end

      context "With valid attributes" do
        let(:apiKey) { ENV["AUTH_SERVER_API_KEY"] }
        let(:company) { create(:company) }
        let(:client) { create(:client) }
        let(:plan) { create(:plan) }
        let!(:agent) { create(:agent) }
        let(:policy) {
          {
            agent_id: agent.id,
            client_id: client.id,
            plan_id: plan.id,
            company_id: company.id,
            effective_date: Faker::Date.birthday(min_age: 2, max_age: 25),
            policy_number: Faker::Number.number(digits: 12),
            renewal_date: Faker::Date.birthday(min_age: 2, max_age: 25),
            option: Faker::Number.number(digits: 4),
            frequency: ["Annual", "Semiannual", "Quarterly", "Monthly"].sample,
            premium: Faker::Number.number(digits: 4),
          }
        }
        it "creates the policy" do
          expect{post "/api/v1/policies", params: policy, headers: { "Api-Key": apiKey }}.to change{Policy.count}.by(1)
        end

        it "creates the renewal" do
          expect{post "/api/v1/policies", params: policy, headers: { "Api-Key": apiKey }}.to change{Renewal.count}.by(2)
        end
      end
    end
  end
end
