class Generator

    def self.date
        Faker::Date.birthday(min_age: 2,max_age:25)
    end

    def self.plan_name
        Faker::App.name
    end
end