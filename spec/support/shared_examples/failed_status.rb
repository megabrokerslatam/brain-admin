shared_examples :failed_status do
  response 401, "Unauthorized Request" do
    let("Api-Key") { 0 }
    run_test!
  end

  response 403, "Forbidden" do
    let("Authorization") { 123 }
    run_test!
  end
end
