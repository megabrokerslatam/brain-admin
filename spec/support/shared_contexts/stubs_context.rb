# frozen_string_literal: true

# Authenticated but no permissions
forbidden = {
  reason: "Usted no cuenta con los permisos para acceder a este recurso",
  state: "forbidden",
  debug: {},
}
#invalid application
invalid_application = {
  reason: "invalid application",
  state: "unauthorized",
  debug: {},
}

shared_context :stubs_context do
  before(:each) do
    # AUTH-SERVER STUBS
    stub_request(:any, /#{ENV["AUTH_SERVER_URL"]}*/).with(headers: { 'Api-Key': ENV["AUTH_SERVER_API_KEY"] }).to_return(status: 200)

    # Stub any request to auth server with wrong api key
    stub_request(:any, /#{ENV["AUTH_SERVER_URL"]}*/).with(headers: { 'Api-Key': "0" }).to_return(
      status: 401,
      body: invalid_application.to_json,
      headers: { "Content-Type" => "application/json" },
    )

    # Stub request for unauthorized
    stub_request(:any, /#{ENV["AUTH_SERVER_URL"]}/).with(headers: { "Authorization" => /\w/ }).to_return(
      status: 403,
      body: forbidden.to_json,
    )

    #SISCOB STUBS

    stub_request(:any, /#{ENV["SISCOB_API_URL"]}.*/).
      with(headers: { "Api-Key" => ENV["AUTH_SERVER_API_KEY"] }).
      to_return(status: 200,
                body: {
                  id: 2,
                  client_id: 27,
                  USD: 100.0,
                  created_at: "2022-11-04T19:08:44.442Z",
                  updated_at: "2022-11-04T19:28:28.531Z", BOB: 0,
                }.to_json, headers: {})
    #stub_request(:get, "#{ENV.fetch('BRAIN_URL', nil)}/api/v1/renewals/1.json").to_return(
    #  status: 200,
    #  body: @stubbed_renewal
    #)

  end
end
