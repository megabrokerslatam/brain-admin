# frozen_string_literal: true

require "rails_helper"

RSpec.configure do |config|
  # Specify a root folder where Swagger JSON files are generated
  # NOTE: If you're using the rswag-api to serve API descriptions, you'll need
  # to ensure that it's configured to serve Swagger from the same folder
  config.swagger_root = Rails.root.join("swagger").to_s

  # Define one or more Swagger documents and provide global metadata for each one
  # When you run the 'rswag:specs:swaggerize' rake task, the complete Swagger will
  # be generated at the provided relative path under swagger_root
  # By default, the operations defined in spec files are added to the first
  # document below. You can override this behavior by adding a swagger_doc tag to the
  # the root example_group in your specs, e.g. describe '...', swagger_doc: 'v2/swagger.json'
  config.swagger_docs = {
    "v1/swagger.yaml" => {
      openapi: "3.0.1",
      info: {
        title: "API V1",
        version: "v1",
      },
      consumes:[
        "application/json",
        "multipart/form-data"
      ],
      produces:[
        "application/json"
      ],
      security: [{ Bearer: [] }, { api_key: [] }],
      components: {
        securitySchemes: {
          api_key: {
            type: :apiKey,
            in: :header,
            name: "Api-Key",
          },
          Bearer: {
            type: :http,
            scheme: :bearer,
          },
        },
        schemas: {
          client_list: {
            type: "object",
            properties: {
              data: {
                type: "array",
                items: {
                  "$ref" => "#/components/schemas/client",
                },
              },
            },
          },
          renewal: {
            type: "object",
            properties: {
              policy_id: { type: :integer },
              plan_id: { type: :integer },
              option: { type: :integer },
              frequency: { type: :string },
              premium: { type: :string },
              renewal_date: { type: :string },
              renewed_on: { type: :string },
              state: { type: :string },
            },
          },
          client: {
            type: "object",
            properties: {
              id: { type: :integer },
              first_name: { type: :string },
              email: { type: :string },
              birthday: { type: :string },
              commercial_executive_id: { type: :integer },
              support_executive_id: { type: :integer },
              hubspot: { type: :integer },
              titular: { type: :boolean },
              age: { type: :integer },
              policies: {
                type: "array",
                items: {
                  "$ref" => "#/components/schemas/policy",
                },
              },

            },
          },
          policy: {
            type: "object",
            properties: {
              id: { type: :integer },
              client_id: { type: :integer },
              policy_number: { type: :string },
              effective_date: { type: :string },
              enabled: { type: :boolean },
              client_name: { type: :string },
              company: { type: :string },
              plan: { type: :string },
              option: { type: :integer },
              policy_type: { type: :integer },
            },
          },
          company: {
            type: "object",
            properties: {
              id: { type: :integer },
              name: { type: :string },
              short_name: { type: :string },
              portal_url: { type: :string },
            },
          },
          pagination: {
            type: "object",
            properties: {
              total_count: { type: :integer },
              items_per_page: { type: :integer },
              current_page: { type: :integer },
              total_pages: { type: :integer },
            },
          },
        },
      },
      paths: {},
      servers: [
        {
          url: "http://{defaultHost}",
          variables: {
            defaultHost: {
              default: "localhost:400",
            },
          },
        },
      ],
    },
  }

  # Specify the format of the output Swagger file when running 'rswag:specs:swaggerize'.
  # The swagger_docs configuration option has the filename including format in
  # the key, this may want to be changed to avoid putting yaml in json files.
  # Defaults to json. Accepts ':json' and ':yaml'.
  config.swagger_format = :yaml
end
