FactoryBot.define do 
    factory :client do
        first_name { Faker::Name.first_name}
        last_name {Faker::Name.last_name}
        phone_number {Faker::PhoneNumber.phone_number}
        email {Faker::Internet.email}
        birthday {Generator.date}
        city {Faker::Address.city}
        country {Faker::Address.country}
    end
end

