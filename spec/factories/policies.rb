FactoryBot.define do 
    factory :policy do
       policy_number {Faker::Number.number(digits: 12)}
       effective_date {Faker::Date.birthday(min_age: 2,max_age:25)}


       enabled {1}
    end
end

