FactoryBot.define do 
    factory :renewal do
       option {Faker::Number.number(digits: 4)}
       premium {Faker::Number.number(digits: 4)}
       renewed_on {Generator.date}
       renewal_date {Generator.date}
       frequency {%w[Annual Semiannual Quarterly Monthly].sample}
    end
end
