FactoryBot.define do 
    factory :company do
       name {Faker::Company.name}
       short_name {Faker::Company.name.split.first}
       portal_url {Faker::Internet.url}
    end
end

