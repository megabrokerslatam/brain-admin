FactoryBot.define do 
    factory :agent do
        first_name { Faker::Name.first_name}
        last_name {Faker::Name.last_name}
        phone_number {Faker::PhoneNumber.phone_number}
        email {Faker::Internet.email}
        birthday {Faker::Date.birthday(min_age: 18,max_age:75)}
        city {Faker::Address.city}
        country {Faker::Address.country}
    end
end

