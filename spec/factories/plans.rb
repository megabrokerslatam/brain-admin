FactoryBot.define do 
    factory :plan do
      association :company
      name {Generator.plan_name}
      joint {Faker::Boolean.boolean(true_ratio: 0.5)}
      enabled {1}
    end
end
