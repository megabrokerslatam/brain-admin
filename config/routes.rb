Rails.application.routes.draw do

  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root to: 'home#index'
  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      resources :companies
      resources :plans, only: [:index,:show]
      resources :users
      resources :renewals
      resources :clients, only: [:index,:show,:create,:update] 
      resources :employees
      resources :agents
      resources :policies do
        resources :renewals, only: :index
      end
      get 'static_data', to: 'api#static'
    end
  end
end
